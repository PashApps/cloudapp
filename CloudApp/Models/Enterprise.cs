﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Angular_ASPNETCore_Seed.Models
{
    public class Enterprise:BaseModel
    {
        public string Name { get; set; }
        public string PrimaryContact { get; set; }
        public string PrimaryContactEmail { get; set; }
        public string PrimaryContactPhone { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }

    public class User : IdentityUser
    {

        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [ScaffoldColumn(false)]
        public int CreatedBy { get; set; }
        [ScaffoldColumn(false)]
        public int ModifiedBy { get; set; }
        [ScaffoldColumn(false)]
        public int Status { get; set; }
        [ScaffoldColumn(false)]
        public DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public DateTime ModifiedDate { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Department { get; set; }
        public int EnterpriseId { get; set; }
    }


    public class Role:BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }

    public class Lookup : BaseModel
    {
        public string Value { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }

    public class UserRole : BaseModel
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }

    public class Application : BaseModel
    {

        public string Name { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
    }
    //Base Model
    //Development
    public class ApplicationAccess : BaseModel
    {
        public int ApplicationId { get; set; }
        public int RoleId { get; set; }
    }
}
